#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <cuda_runtime.h>
#include <time.h>

/* Vector sum on host
 * Function declaration is same as __host__ void cpuVectorSum(arguments)
 * adds two vectors a, b and stores result in c
 */
void cpuVectorSum(int * __restrict__ a, int * __restrict__ b, int * __restrict__ c, int N)
{
    for (int i = 0; i < N; ++i){
        c[i] = a[i] + b[i];
    }
}

/* Vector sum on device 
 * Arguments are pointers to device memory space
 * __restrict__ means that memory doesnt overlap
 */
__global__ void gpuVectorSum(int * __restrict__ a, int * __restrict__ b, int * __restrict__ c, int N)
{
    int idx = threadIdx.x + blockIdx.x * blockDim.x;

    /* check array bounds */
    if (idx < N){
        c[idx] = a[idx] + b[idx];
    }
}

/* fill array with random numbers from 0 to 100 */
__host__ void randomFill(int *arr, int N)
{
    for (int i = 0; i < N; ++i) {
        arr[i] = rand() % 100;
    }
}

/* check if two arrays are equal */
bool compare(int *a, int *b, int N)
{
    int equal = true;
    for (int i = 0; i < N; ++i){
        if (a[i] != b[i]){
            equal = false;
            break;
        }
    }
    return equal;
}

int main()
{
    srand(time(NULL));
    /* Array size */
    int N = 10000;
    /* threads per block */
    int threads = 128;
    /* Make sure we run enough threads to proceed vectors of size N */
    int blocks = (N + threads - 1) / threads;

    /* Pointers with prefix h_ means array used on host side
     * d_ * on device side
     */
    int *h_a, *h_b, *h_c;
    int *d_a, *d_b, *d_c;
    int *h_result;

    /* allocate memory on host */
    size_t size_i = N * sizeof(int);
    h_a = (int *) malloc(size_i);
    h_b = (int *) malloc(size_i);
    h_c = (int *) malloc(size_i);
    h_result = (int *) malloc(size_i);

    /* allocate memory on device */
    cudaMalloc((void **) &d_a, size_i);
    cudaMalloc((void **) &d_b, size_i);
    cudaMalloc((void **) &d_c, size_i);

    /* fill arrays with random numbers */
    randomFill(h_a, N);
    randomFill(h_b, N);

    /* copy arrays from host to device */
    cudaMemcpy(d_a, h_a, size_i, cudaMemcpyHostToDevice);
    cudaMemcpy(d_b, h_b, size_i, cudaMemcpyHostToDevice);

    /* run sum on gpu */
    gpuVectorSum<<<blocks, threads>>>(d_a, d_b, d_c, N);

    /* Copy results back to host */
    cudaMemcpy(h_c, d_c, size_i, cudaMemcpyDeviceToHost);
    
    /* run sum on host */
    cpuVectorSum(h_a, h_b, h_result, N);

    /* compare host and device results */
    bool equal = compare(h_c, h_result, N);

    if (equal){
        printf("Arrays are equal\n");
    } else {
        printf("Arrays are not equal\n");
    }

    /* free memory */
    free(h_a);
    free(h_b);
    free(h_c);
    free(h_result);

    cudaFree(d_a);
    cudaFree(d_b);
    cudaFree(d_c);
}