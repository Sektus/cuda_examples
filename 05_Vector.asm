	.file	"05_Vector.c"
	.text
	.p2align 4,,15
	.type	cpuVectorSum._omp_fn.0, @function
cpuVectorSum._omp_fn.0:
.LFB2:
	.cfi_startproc
	leaq	8(%rsp), %r10
	.cfi_def_cfa 10, 0
	andq	$-32, %rsp
	pushq	-8(%r10)
	pushq	%rbp
	.cfi_escape 0x10,0x6,0x2,0x76,0
	movq	%rsp, %rbp
	pushq	%r15
	pushq	%r14
	pushq	%r13
	pushq	%r12
	.cfi_escape 0x10,0xf,0x2,0x76,0x78
	.cfi_escape 0x10,0xe,0x2,0x76,0x70
	.cfi_escape 0x10,0xd,0x2,0x76,0x68
	.cfi_escape 0x10,0xc,0x2,0x76,0x60
	movq	%rdi, %r12
	pushq	%r10
	.cfi_escape 0xf,0x3,0x76,0x58,0x6
	pushq	%rbx
	.cfi_escape 0x10,0x3,0x2,0x76,0x50
	movl	24(%rdi), %ebx
	call	omp_get_num_threads@PLT
	movl	%eax, %r13d
	call	omp_get_thread_num@PLT
	movl	%eax, %ecx
	movl	%ebx, %eax
	cltd
	idivl	%r13d
	cmpl	%edx, %ecx
	jge	.L12
	addl	$1, %eax
	xorl	%edx, %edx
.L12:
	imull	%eax, %ecx
	addl	%ecx, %edx
	addl	%edx, %eax
	cmpl	%eax, %edx
	jge	.L13
	movq	(%r12), %r8
	movslq	%edx, %r13
	movl	%eax, %edi
	subl	%edx, %edi
	movq	16(%r12), %r9
	movq	8(%r12), %r10
	leal	-1(%rdi), %r14d
	leaq	(%r8,%r13,4), %rsi
	movq	%rsi, %rcx
	shrq	$2, %rcx
	negq	%rcx
	andl	$7, %ecx
	leal	7(%rcx), %r11d
	cmpl	%r11d, %r14d
	jb	.L6
	testl	%ecx, %ecx
	je	.L14
	movl	(%rsi), %esi
	addl	(%r10,%r13,4), %esi
	cmpl	$1, %ecx
	movl	%esi, (%r9,%r13,4)
	leal	1(%rdx), %esi
	je	.L7
	movslq	%esi, %rsi
	movl	(%r10,%rsi,4), %r11d
	addl	(%r8,%rsi,4), %r11d
	cmpl	$2, %ecx
	movl	%r11d, (%r9,%rsi,4)
	leal	2(%rdx), %esi
	je	.L7
	movslq	%esi, %rsi
	movl	(%r10,%rsi,4), %r11d
	addl	(%r8,%rsi,4), %r11d
	cmpl	$3, %ecx
	movl	%r11d, (%r9,%rsi,4)
	leal	3(%rdx), %esi
	je	.L7
	movslq	%esi, %rsi
	movl	(%r10,%rsi,4), %r11d
	addl	(%r8,%rsi,4), %r11d
	cmpl	$4, %ecx
	movl	%r11d, (%r9,%rsi,4)
	leal	4(%rdx), %esi
	je	.L7
	movslq	%esi, %rsi
	movl	(%r10,%rsi,4), %r11d
	addl	(%r8,%rsi,4), %r11d
	cmpl	$5, %ecx
	movl	%r11d, (%r9,%rsi,4)
	leal	5(%rdx), %esi
	je	.L7
	movslq	%esi, %rsi
	movl	(%r10,%rsi,4), %r11d
	addl	(%r8,%rsi,4), %r11d
	cmpl	$7, %ecx
	movl	%r11d, (%r9,%rsi,4)
	leal	6(%rdx), %esi
	jne	.L7
	movslq	%esi, %rsi
	movl	(%r8,%rsi,4), %r11d
	addl	(%r10,%rsi,4), %r11d
	movl	%r11d, (%r9,%rsi,4)
	leal	7(%rdx), %esi
.L7:
	subl	%ecx, %edi
	addq	%r13, %rcx
	xorl	%edx, %edx
	salq	$2, %rcx
	movl	%edi, %r11d
	xorl	%r15d, %r15d
	leaq	(%r8,%rcx), %r14
	leaq	(%r10,%rcx), %r13
	shrl	$3, %r11d
	addq	%r9, %rcx
	.p2align 4,,10
	.p2align 3
.L9:
	vmovdqu	0(%r13,%rdx), %xmm0
	addl	$1, %r15d
	vinserti128	$0x1, 16(%r13,%rdx), %ymm0, %ymm0
	vpaddd	(%r14,%rdx), %ymm0, %ymm0
	vmovups	%xmm0, (%rcx,%rdx)
	vextracti128	$0x1, %ymm0, 16(%rcx,%rdx)
	addq	$32, %rdx
	cmpl	%r11d, %r15d
	jb	.L9
	movl	%edi, %ecx
	andl	$-8, %ecx
	cmpl	%ecx, %edi
	leal	(%rcx,%rsi), %edx
	je	.L32
	movslq	%edx, %r13
	vzeroupper
.L6:
	movl	(%r10,%r13,4), %ecx
	addl	(%r8,%r13,4), %ecx
	movl	%ecx, (%r9,%r13,4)
	leal	1(%rdx), %ecx
	cmpl	%ecx, %eax
	jle	.L10
	movslq	%ecx, %rcx
	movl	(%r10,%rcx,4), %esi
	addl	(%r8,%rcx,4), %esi
	movl	%esi, (%r9,%rcx,4)
	leal	2(%rdx), %ecx
	cmpl	%ecx, %eax
	jle	.L10
	movslq	%ecx, %rcx
	movl	(%r10,%rcx,4), %esi
	addl	(%r8,%rcx,4), %esi
	movl	%esi, (%r9,%rcx,4)
	leal	3(%rdx), %ecx
	cmpl	%ecx, %eax
	jle	.L10
	movslq	%ecx, %rcx
	movl	(%r10,%rcx,4), %esi
	addl	(%r8,%rcx,4), %esi
	movl	%esi, (%r9,%rcx,4)
	leal	4(%rdx), %ecx
	cmpl	%ecx, %eax
	jle	.L10
	movslq	%ecx, %rcx
	movl	(%r10,%rcx,4), %esi
	addl	(%r8,%rcx,4), %esi
	movl	%esi, (%r9,%rcx,4)
	leal	5(%rdx), %ecx
	cmpl	%ecx, %eax
	jle	.L10
	movslq	%ecx, %rcx
	movl	(%r10,%rcx,4), %esi
	addl	(%r8,%rcx,4), %esi
	movl	%esi, (%r9,%rcx,4)
	leal	6(%rdx), %ecx
	cmpl	%ecx, %eax
	jle	.L10
	movslq	%ecx, %rcx
	movl	(%r10,%rcx,4), %esi
	addl	(%r8,%rcx,4), %esi
	movl	%esi, (%r9,%rcx,4)
	leal	7(%rdx), %ecx
	cmpl	%ecx, %eax
	jle	.L10
	movslq	%ecx, %rcx
	movl	(%r10,%rcx,4), %esi
	addl	(%r8,%rcx,4), %esi
	movl	%esi, (%r9,%rcx,4)
	leal	8(%rdx), %ecx
	cmpl	%ecx, %eax
	jle	.L10
	movslq	%ecx, %rcx
	movl	(%r10,%rcx,4), %esi
	addl	(%r8,%rcx,4), %esi
	movl	%esi, (%r9,%rcx,4)
	leal	9(%rdx), %ecx
	cmpl	%ecx, %eax
	jle	.L10
	movslq	%ecx, %rcx
	movl	(%r10,%rcx,4), %esi
	addl	(%r8,%rcx,4), %esi
	movl	%esi, (%r9,%rcx,4)
	leal	10(%rdx), %ecx
	cmpl	%ecx, %eax
	jle	.L10
	movslq	%ecx, %rcx
	movl	(%r10,%rcx,4), %esi
	addl	(%r8,%rcx,4), %esi
	movl	%esi, (%r9,%rcx,4)
	leal	11(%rdx), %ecx
	cmpl	%ecx, %eax
	jle	.L10
	movslq	%ecx, %rcx
	movl	(%r10,%rcx,4), %esi
	addl	(%r8,%rcx,4), %esi
	movl	%esi, (%r9,%rcx,4)
	leal	12(%rdx), %ecx
	cmpl	%ecx, %eax
	jle	.L10
	movslq	%ecx, %rcx
	addl	$13, %edx
	movl	(%r10,%rcx,4), %esi
	addl	(%r8,%rcx,4), %esi
	cmpl	%edx, %eax
	movl	%esi, (%r9,%rcx,4)
	jle	.L10
	movslq	%edx, %rdx
	movl	(%r10,%rdx,4), %ecx
	addl	(%r8,%rdx,4), %ecx
	movl	%ecx, (%r9,%rdx,4)
	.p2align 4,,10
	.p2align 3
.L10:
	cmpl	%eax, %ebx
	jne	.L3
.L5:
	movl	%ebx, 28(%r12)
	jmp	.L33
	.p2align 4,,10
	.p2align 3
.L13:
	xorl	%eax, %eax
.L3:
	cmpl	%ebx, %eax
	je	.L5
.L33:
	popq	%rbx
	popq	%r10
	.cfi_remember_state
	.cfi_def_cfa 10, 0
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	leaq	-8(%r10), %rsp
	.cfi_def_cfa 7, 8
	ret
	.p2align 4,,10
	.p2align 3
.L14:
	.cfi_restore_state
	movl	%edx, %esi
	jmp	.L7
	.p2align 4,,10
	.p2align 3
.L32:
	vzeroupper
	jmp	.L10
	.cfi_endproc
.LFE2:
	.size	cpuVectorSum._omp_fn.0, .-cpuVectorSum._omp_fn.0
	.p2align 4,,15
	.globl	cpuVectorSum
	.type	cpuVectorSum, @function
cpuVectorSum:
.LFB0:
	.cfi_startproc
	subq	$72, %rsp
	.cfi_def_cfa_offset 80
	movq	%rdi, 8(%rsp)
	leaq	cpuVectorSum._omp_fn.0(%rip), %rdi
	movl	%ecx, 40(%rsp)
	vmovq	8(%rsp), %xmm1
	movq	%rdx, 32(%rsp)
	vpinsrq	$1, %rsi, %xmm1, %xmm0
	leaq	16(%rsp), %rsi
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	movq	%fs:40, %rax
	movq	%rax, 56(%rsp)
	xorl	%eax, %eax
	vmovaps	%xmm0, 16(%rsp)
	call	GOMP_parallel@PLT
	movq	56(%rsp), %rax
	xorq	%fs:40, %rax
	jne	.L40
	addq	$72, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
.L40:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE0:
	.size	cpuVectorSum, .-cpuVectorSum
	.section	.text.startup,"ax",@progbits
	.p2align 4,,15
	.globl	main
	.type	main, @function
main:
.LFB1:
	.cfi_startproc
	subq	$840, %rsp
	.cfi_def_cfa_offset 848
	movq	%fs:40, %rax
	movq	%rax, 824(%rsp)
	xorl	%eax, %eax
	leaq	48(%rsp), %rcx
	leaq	304(%rsp), %rsi
	jmp	.L42
	.p2align 4,,10
	.p2align 3
.L50:
	movl	-4(%rcx,%rax,4), %edx
	movl	%edx, (%rcx,%rax,4)
	imull	%eax, %edx
	cmpl	$63, %eax
	movl	%edx, (%rsi,%rax,4)
	je	.L49
.L44:
	addq	$1, %rax
.L42:
	testq	%rax, %rax
	jne	.L50
	movl	$0, (%rcx)
	movl	$0, (%rsi)
	jmp	.L44
	.p2align 4,,10
	.p2align 3
.L49:
	movq	%rcx, 8(%rsp)
	leaq	560(%rsp), %rax
	leaq	cpuVectorSum._omp_fn.0(%rip), %rdi
	vmovq	8(%rsp), %xmm1
	xorl	%ecx, %ecx
	vpinsrq	$1, %rsi, %xmm1, %xmm0
	leaq	16(%rsp), %rsi
	xorl	%edx, %edx
	movq	%rax, 32(%rsp)
	movl	$64, 40(%rsp)
	vmovaps	%xmm0, 16(%rsp)
	call	GOMP_parallel@PLT
	xorl	%eax, %eax
	movq	824(%rsp), %rdi
	xorq	%fs:40, %rdi
	jne	.L51
	addq	$840, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 8
	ret
.L51:
	.cfi_restore_state
	call	__stack_chk_fail@PLT
	.cfi_endproc
.LFE1:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 7.5.0-3ubuntu1~18.04) 7.5.0"
	.section	.note.GNU-stack,"",@progbits
