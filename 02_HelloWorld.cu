#include <stdio.h>
#include <cuda_runtime.h>

__global__ void kernel() 
{
    /* Thread index calculation in 1D block 
     * threaIdx blockIdx blockDim gridIdx gridDim - are defined by cuda runtime
     * during kernle invocation depending on grid size and have type dim3
     * which have x, y, z members and defines number of threads 
     * that correspond to specific axes.
     * In this example we have (10, 1, 1) blocks of size (1, 1, 1)
    */
    int idx = threadIdx.x + blockIdx.x * blockDim.x;
    printf("Hello World! from thread = %d\n", idx);
}

int main()
{
    int N = 10; // number of blocks
    /* Kernel with N blocks invocation */
    kernel<<<N, 1>>>();
    cudaDeviceSynchronize();
    return 0;
}