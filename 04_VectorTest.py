#!/usr/bin/python3
import matplotlib.pyplot as plt 
import pandas as pd 
import numpy as np

plt.style.use("ggplot")

df = pd.read_csv("test.csv", sep=" ");
x = np.log2(df["N"])

fig = plt.figure(figsize=[16/2, 9 / 2])
plt.plot(x, df["GPU"]);
plt.plot(x, df["CPU"]);
plt.yscale("log")

plt.legend(["GPU", "CPU"])
plt.xlabel("Vector size")
plt.ylabel("time(ms)")
plt.title("VectorSum")
plt.savefig("VectorSum.png")
