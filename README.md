# Requirements
- Python3
- Pandas
- Matplotlib
- CUDA
- Make
- OpenMP

# Description
CUDA programming examples.

# Files 

* cuda.pdf - presentation in Polish language

* 01_HelloWorld.cu - basic hello world example
```bash
./01_HelloWorld
#output
Hello World!
```
* 02_HelloWorld.cu - hello world with multithreading
```bash
./02_HelloWorld
#output
Hello World! from thread = 9
Hello World! from thread = 3
Hello World! from thread = 2
Hello World! from thread = 0
Hello World! from thread = 1
Hello World! from thread = 7
Hello World! from thread = 6
Hello World! from thread = 8
Hello World! from thread = 5
Hello World! from thread = 4
```

* 03_Vector.cu - vector addition example
```bash
./03_Vector
#output
Arrays are equal
```

* 04_VectorTest.cu, 04_VectorTest.py - test speed of single CPU core vs GPU
* 05_Vector_openMP.c, 05_Vector_OpenMP.py - test speed of CPU (using openMP with enabled simd(avx2)) vs GPU
* 05_Vector.c - only host part of previous code.
* 05_Vector.asm - generated assembler host code for 05_Vector.c
* 05_Vector_openMP.ptx - generated PTX device code for 05_Vector_openMP.c 
* Makefile - code compilation
* .csv .png - test results

# Usage 

```bash
make #build examples
```
```bash
make test #repeat tests
```
```bash
make assembler #generate assembly and ptx for example 05
```

# Results 

Intel(R) Core(TM) i5-7200U CPU @ 2.50GHz vs NVIDIA geForce 940MX

![Single core vs GPU](VectorSum.png)
![SIMD PARALLEL CPU vs GPU](VectorSumOpenMP.png)
