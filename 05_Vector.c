#include <math.h>
#include <omp.h>

#define ELEMS 4 * 16

void cpuVectorSum(int * restrict a, int * restrict b, int * restrict c, int N)
{
    int i;
    #pragma omp parallel for simd simdlen(16) linear(i:1)
    for (i = 0; i < N; ++i){
        c[i] = a[i] + b[i];
    }
}

int main(){
    int a[ELEMS], b[ELEMS], c[ELEMS];
    for(int i = 0; i < ELEMS; ++i){
        a[i] = i * 2 + i == 0 ? 0 : a[i - 1];
        b[i] = i * a[i];
    }

    cpuVectorSum(a, b, c, ELEMS);
    return 0;
}