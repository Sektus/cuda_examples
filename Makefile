NVCC = nvcc
NVCCFLAGS = -arch=compute_50 -code=sm_50 -x cu
COMPILE.cu = $(NVCC) $(NVCCFLAGS)

SRCS = $(shell find ./ -type f -name "*.cu")
TARGETS = $(SRCS:%.cu=%)

all: $(TARGETS) 05_Vector_openMP 05_Vector_openMP.ptx 05_Vector.asm

%: %.cu
	$(COMPILE.cu) $< -o $@ -lm

%: %.c 
	$(COMPILE.cu) -Xcompiler '-O3 -fopenmp -mavx2' $< -o $@ -lm

assembler: 05_Vector_openMP.ptx 05_Vector.asm

05_Vector.asm: 05_Vector.c 
	gcc -S -O3 -fopenmp -mavx2 $< -o $@

05_Vector_openMP.ptx: 05_Vector_openMP.c 
	$(COMPILE.cu)  -ptx -Xcompiler '-O3 -fopenmp -mavx2' $< -o $@ -lm

test: 04_VectorTest 05_Vector_openMP
	./04_VectorTest > test.csv
	python3 04_VectorTest.py
	./05_Vector_openMP > test_2.csv
	python3 05_VectorTest.py

clean:
	$(RM) $(TARGETS) 05_Vector_openMP test.csv test_2.csv

assembler_clean:
	$(RM) 05_Vector_openMP.ptx 05_Vector.asm
