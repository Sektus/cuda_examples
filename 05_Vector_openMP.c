#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <cuda_runtime.h>
#include <math.h>
#include <omp.h>

/* cpu code with openmp */
void cpuVectorSum(int * __restrict__ a, int * __restrict__ b, int * __restrict__ c, int N)
{
    int i;
    #pragma omp parallel for simd simdlen(16) linear(i:1)
    for (i = 0; i < N; ++i){
        c[i] = a[i] + b[i];
    }
}

__global__ void gpuVectorSum(int * __restrict__ a, int * __restrict__ b, int * __restrict__ c, int N)
{
    int idx = threadIdx.x + blockIdx.x * blockDim.x;

    if (idx < N){
        c[idx] = a[idx] + b[idx];
    }
}

__host__ void randomFill(int *arr, int N)
{
    for (int i = 0; i < N; ++i) {
        arr[i] = rand() % 100;
    }
}

void test(int N){
    int threads = 128;
    int blocks = (N + threads - 1) / threads;

    int *h_a, *h_b, *h_c;
    int *d_a, *d_b, *d_c;
    int *h_result;

    size_t size_i = N * sizeof(int);
    h_a = (int *) malloc(size_i);
    h_b = (int *) malloc(size_i);
    h_c = (int *) malloc(size_i);
    h_result = (int *) malloc(size_i);

    cudaMalloc((void **) &d_a, size_i);
    cudaMalloc((void **) &d_b, size_i);
    cudaMalloc((void **) &d_c, size_i);

    randomFill(h_a, N);
    randomFill(h_b, N);

    cudaMemcpy(d_a, h_a, size_i, cudaMemcpyHostToDevice);
    cudaMemcpy(d_b, h_b, size_i, cudaMemcpyHostToDevice);

    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    cudaEventRecord(start);
    gpuVectorSum<<<blocks, threads>>>(d_a, d_b, d_c, N);
    cudaEventRecord(stop);

    cudaMemcpy(h_c, d_c, size_i, cudaMemcpyDeviceToHost);
    cudaEventSynchronize(stop);
    float gpu = 0.0f;
    cudaEventElapsedTime(&gpu, start, stop);

    cudaEventRecord(start);
    cpuVectorSum(h_a, h_b, h_result, N);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    float cpu = 0.0f;
    cudaEventElapsedTime(&cpu, start, stop);

    printf("%d %f %f\n", N, gpu, cpu);
    free(h_a);
    free(h_b);
    free(h_c);
    free(h_result);

    cudaFree(d_a);
    cudaFree(d_b);
    cudaFree(d_c);

    cudaEventDestroy(start);
    cudaEventDestroy(stop);
}

int main()
{
    srand(time(NULL));
    int N = 0;
    
    printf("N GPU CPU\n");
    for (int i = 4; i < 25; ++i){
        N = (int)powf(2, i);
        test(N);
    }
    return 0;
}