#include <stdio.h>
#include <cuda_runtime.h>

/* Kernel function
 * __global__ specifier defines kernel function
 * that is executed on device and can be invoced 
 * with function<<<grid, block>>>(arguments); syntax
 * should have void return type
 */
__global__ void kernel() 
{
    printf("Hello World!\n");
}

int main()
{
    /* Kernel function invocation */
    kernel<<<1, 1>>>();
    /* We should synchronize device to get result from gpu, because
    kernels invocation is asynchronous, so control returns to host
    and program ends before function returns. Some functions such as cudaMalloc
    make synchronization implicity*/
    cudaDeviceSynchronize();
    return 0;
}